FROM maven:3.6.2-jdk-8-slim
MAINTAINER Martin Humpolec <kbc@htns.cz>

ENV APP_VERSION 1.1.0
# install git
RUN apt-get update -y
RUN apt-get install -y git-core


COPY . /code/
WORKDIR /code/

RUN git clone -b 2.1.19 https://mhumpolec@bitbucket.org/mhumpolec/kbc-salesforce-extractor.git ./code/ 
RUN mvn compile

# https://github.com/keboola/docker-bundle/issues/198
RUN chmod a+rw ./ -R
RUN mkdir -p /var/maven/.m2 && chmod a+rw /var/maven -R

# https://github.com/carlossg/docker-maven#running-as-non-root
ENV MAVEN_CONFIG=/var/maven/.m2

# trying to use desner approach of entrypoint
# CMD mvn -q exec:java -Dexec.args=/data  

ENTRYPOINT mvn -q -e exec:java -Dexec.args=/data  