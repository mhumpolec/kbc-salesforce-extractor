# Salesforce Extractor #

Salesforce Extractor component for Keboola Connection.

### Functionality ###

The component export data from Salesforce based on SOQL you provide and save it into the out/tables directory in file named as *object*.csv. 

### Configuration ###
#### Parameters ####

* Loginname - (REQ) your user name, when exporting data from sandbox don't forget to add .sandboxname at the end
* Password - (REQ) your password
* Security Token - (REQ) your security token, don't forget it is different for sandbox
* sandbox - (REQ) true when you want to export data from sandbox
* sinceLast - true when you want to extract incremental updates from last run, when true you need to include the ID column in SOQL as it is set as primary key
* objects - (REQ) specify object and (optional) SOQL from which you want to export data. You can specify multiple objects separated by comma (on one line), obviously without specific SOQLs

As of Spring '17 release this connector allow querying for deleted records (use IsDeleted). 