package keboola.salesforce.extractor; 

import java.io.*;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.util.*;


import com.sforce.async.*;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.soap.partner.*;
import com.sforce.ws.ConnectionException;
import com.sforce.ws.ConnectorConfig;
import com.sun.org.apache.bcel.internal.classfile.Field;
import com.sforce.soap.partner.DescribeSObjectResult; 

import keboola.salesforce.extractor.config.JsonConfigParser;
import keboola.salesforce.extractor.config.KBCConfig;
import keboola.salesforce.extractor.config.JsonlStateWriter;
import keboola.salesforce.extractor.config.LastState;
import keboola.salesforce.extractor.config.ManifestBuilder;
import keboola.salesforce.extractor.config.ManifestFile;


/**
 *
 * @author David Esner <esnerda at gmail.com>
 * @author Martin Humpolec <martin.humpolec at gmail.com>
 * @created 2016 
 */
public class Extractor {
//
	
	Calendar serverTime;
	
	public static void main(String[] args) throws AsyncApiException, ConnectionException, IOException   {
		if (args.length == 0) {
			System.err.println("No parameters provided.");
			System.exit(1);
		}

		String dataPath = args[0];
		String outTablesPath = dataPath + File.separator + "out" + File.separator + "tables" + File.separator;
		
		KBCConfig config = null;
		File confFile = new File(args[0] + File.separator + "config.json");
		if (!confFile.exists()) {
			System.out.println("config.json does not exist!");
			System.err.println("config.json does not exist!");
			System.exit(1);
		}
		// Parse config file
		try {
			if (confFile.exists() && !confFile.isDirectory()) {
				config = JsonConfigParser.parseFile(confFile);
			}
		} catch (Exception ex) {
			System.out.println("Failed to parse config file");
			System.err.println(ex.getMessage());
			System.exit(1);
		}
		if (!config.validate()) {
			System.out.println(config.getValidationError());
			System.err.println(config.getValidationError());
			System.exit(1);
		}		
		
			LastState lastState = retrieveStateFile(dataPath);		
			Map<String, Date> lastBulkRequests = null;
			Calendar lastSync = null;
			if (lastState != null) {
		            try {
		                lastBulkRequests = lastState.getBulkRequests();
		            } catch (NullPointerException ex) {
		                System.out.println("No matching state.");
		            }
		    }
			LastState newState = new LastState(new HashMap());
			
			if( config.getParams().getObjects() == null){
				System.err.println( "No object to extract specified.");
				System.exit(1);
			}

		System.out.println( "Everything ready, let's get some data from Salesforce, loginname: " + config.getParams().getLoginname());
   		
		Extractor sfdown = new Extractor();

		sfdown.runQueries( config.getParams().getLoginname(), config.getParams().getPassword() + config.getParams().getSecuritytoken(), config.getParams().getSandbox(), 
				outTablesPath, config.getParams().getObjects(), lastBulkRequests, lastState, newState, config.getParams().getSinceLast());
	
		/*Write state file*/
	        try {
	            JsonlStateWriter.writeStateFile(dataPath + File.separator + "out" + File.separator + "state.json", newState);
	        } catch (IOException ex) {
	            System.err.println("Error building state file " + ex.getMessage());
	            System.exit(1);
	        }
        
   		System.out.println( "All done.");
	}

/**
 * generate SELECT * for the object
 */
	public String getSOQL( String object, PartnerConnection connection)
	{
		String soql = "";   
		try {
		        // Make the describe call
		    	DescribeSObjectResult describeSObjectResult = connection.describeSObject( object);
			        
		        // Get sObject metadata 
		        if (describeSObjectResult != null) {

			        // Get the fields
		        	com.sforce.soap.partner.Field[] fields = describeSObjectResult.getFields();

			        // Iterate through each field and gets its properties 
			        for (int i = 0; i < fields.length; i++) {
			        	com.sforce.soap.partner.Field field = fields[i];
//			       		System.out.println( field.getName() + " - " + field.getType());
	 
			       		if( field.getType() != com.sforce.soap.partner.FieldType.address && field.getType() != com.sforce.soap.partner.FieldType.location && field.getType() != com.sforce.soap.partner.FieldType.base64){
			       			
			          // if not formula field publish it
				          if( soql == ""){
				        	  soql = field.getName();
				          } else {
				        	  soql = soql + "," + field.getName();
				          }
				         }
	
				    }
				  }
				} catch (ConnectionException ce) {
				    ce.printStackTrace();
		    }
		    if( soql != "") {
		    	soql = "SELECT " + soql + " FROM " + object;
		    }
   		// System.out.println( "SOQL: " + soql);

	    return soql;
	}
	

		/**
	 * Creates a Bulk API job and uploads batches for a CSV file.
	 */
	public int runQueries( String loginname, String password, Boolean sandbox, String filesDirectory, List<keboola.salesforce.extractor.config.ObjectsClass> objects, Map<String, Date> lastBulkRequests, LastState lastState, LastState newState, Boolean getSinceLast)
			throws AsyncApiException, ConnectionException, IOException {
		BulkConnection bulkconnection = getBulkConnection( loginname, password, sandbox);
		PartnerConnection connection = getConnection( loginname, password, sandbox);
		
    	if (connection != null) {
    	            
            for(keboola.salesforce.extractor.config.ObjectsClass object : objects){	
            	
  	    		String sname = object.getName(); 
   	        	if( sname.indexOf(',')>0){
   	    			List<String> oos = Arrays.asList(sname.split("\\s*,\\s*"));
   	    			for( String os : oos) {
  	        			String soql = getSOQL( os, connection);
	   	    			System.out.println( "SOQL: " + soql);
   	   	    			runQuery( bulkconnection, filesDirectory, os, soql, lastBulkRequests, lastState, newState, getSinceLast );	            	   	        		   	    				
   	    			}
   	        	} else {
   	        		String soql = object.getSoql(); 
   	            	if( soql == "" || soql == null){
   	        			soql = getSOQL( object.getName(), connection);
   	            	}	
   	    			System.out.println( "SOQL: " + soql);
   	    			runQuery( bulkconnection, filesDirectory, object.getName(), soql , lastBulkRequests, lastState, newState, getSinceLast);	            	   	        		
   	        	}
            }
    	}
    	return 0;
	}

		/**
	 * Creates a Bulk API job and download batches for a CSV file.
	 */
	public int runQuery( BulkConnection bulkconnection, String filesDirectory, String object, String soql, Map<String, Date> lastBulkRequests, LastState lastState, LastState newState, Boolean getSinceLast)
			throws AsyncApiException, ConnectionException, IOException {
		
		try {
			Date lastRun = serverTime.getInstance().getTime();
			
			Calendar lastSync = null;
        	if (lastBulkRequests != null && getSinceLast) {
	              lastSync = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
	              Date dt = lastBulkRequests.get( object);
	              SimpleDateFormat formatter, formatter2;

	              formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
	              formatter2 = new SimpleDateFormat( "hh:mm:ss", Locale.US);
	              
	              if (dt != null) {
	                  // we have last run time, update soql
	            	  lastSync.setTime(dt);
	                  if( soql.toUpperCase().indexOf( " WHERE ") > 0){
	                	  soql = soql + " AND LastModifiedDate >= " + formatter.format( dt) + "T" + formatter2.format( dt) + "Z";
	                  } else {
	                	  soql = soql + " where LastModifiedDate >= " + formatter.format( dt) + "T" + formatter2.format( dt) + "Z";
	                  }
	                  System.out.println( "Modified SOQL: " + soql);
	              }
	           }
 // save state every run           if( getSinceLast) { 
            	newState.getBulkRequests().put( object, lastRun);
 //           }
	   		System.out.println( "Processing object: " + object);
			JobInfo job = new JobInfo();
			job.setObject(object);

			job.setOperation(OperationEnum.queryAll);
			job.setConcurrencyMode(ConcurrencyMode.Parallel);
			job.setContentType(ContentType.CSV);

			job = bulkconnection.createJob(job);
			assert job.getId() != null;

			job = bulkconnection.getJobStatus(job.getId());

			BatchInfo info = null;
			ByteArrayInputStream bout = new ByteArrayInputStream( soql.getBytes());
			info = bulkconnection.createBatchFromStream(job, bout);

			String[] queryResults = null;

			double resultsNr = 0;
			for(int i=1; i<60000; i++) {
//				Thread.sleep(i==0 ? 30 * 1000 : 30 * 1000); //30 sec
				Thread.sleep(i<12 ? i * 1000 * 5 : 60 * 1000); //60 sec
				info = bulkconnection.getBatchInfo(job.getId(),	info.getId());

				if (info.getState() == BatchStateEnum.Completed) {
			   		System.out.println( "Completed, getting results.");
					QueryResultList list = bulkconnection.getQueryResultList(job.getId(), info.getId());
					queryResults = list.getResult();
					System.out.println( "Processed " + info.getNumberRecordsProcessed() + " records.");
					resultsNr = info.getNumberRecordsProcessed();
					break;
				} else if (info.getState() == BatchStateEnum.Failed) {
					System.err.println("-------------- failed ----------" + info);
					bulkconnection.closeJob(job.getId());
					System.exit(1);
				} else {
					System.out.println("-------------- waiting " + ( i < 12 ? i * 5 : 60 ) + " seconds ----------"  /* + info */);
				}
			}

			if (queryResults != null && resultsNr > 0) {
		   		System.out.println( "Write everything into " + filesDirectory + object + ".csv");
		   		Boolean firstFile = true;
				for (String resultId : queryResults) {
					//grabs result stream and passes it to csv writer
					FileHandler.writeCSVFromStream(bulkconnection.getQueryResultStream(job.getId(),	info.getId(), resultId),object, filesDirectory, firstFile);
					//grabs results to ensure integrity
					bulkconnection.getQueryResultList(job.getId(), info.getId()).getResult();
					firstFile = false;
				}
		   		System.out.println( "Close file");

				//notify user of job complete
				//return number of records complete for data check and close job
				int out = info.getNumberRecordsProcessed();
				bulkconnection.closeJob(job.getId());
				System.out.println( "Processed " + out + " records.");
				
				if( getSinceLast) {
					ManifestFile manFile = new ManifestFile.Builder( object + ".csv")
						.setIncrementalLoad(true).setPrimaryKey(new String[]{"Id"}).setDelimiter(",").setEnclosure("\"")
						.build();

					try {
						ManifestBuilder.buildManifestFile(manFile, filesDirectory, object + ".csv");
					} catch (IOException ex) {
						System.err.println("Error building manifest file " + ex.getMessage());
						System.exit(2);
					}
				}

				
				return out;
			}
		} catch (AsyncApiException aae) {
			aae.printStackTrace();
			System.err.println( "AsyncApiException" + aae.getStackTrace()[0].getLineNumber());
			System.exit(1);
		} catch (InterruptedException ie) {
			ie.printStackTrace();
			System.err.println("InterruptedException");
			System.exit(1);
		}
		//something went wrong here, return 0 to catch an error back in main
		return 0;

		
	}

	/**
	 * Create the BulkConnection used to call Bulk API operations.
	 */
	private BulkConnection getBulkConnection(String userName, String password, boolean sandbox)
			throws ConnectionException, AsyncApiException {
		try {
			ConnectorConfig partnerConfig = new ConnectorConfig();
			partnerConfig.setUsername(userName);
			partnerConfig.setPassword(password);
			if ( sandbox == true)  {
				System.out.println("Connecting to Salesforce Sandbox (Bulk API)");
				partnerConfig.setAuthEndpoint("https://test.salesforce.com/services/Soap/u/39.0");
			} else {
				System.out.println("Connecting to Salesforce Production (Bulk API)");
				partnerConfig.setAuthEndpoint("https://login.salesforce.com/services/Soap/u/39.0");			
			}
			// Creating the connection automatically handles login and stores
			// the session in partnerConfig
			new PartnerConnection(partnerConfig);
			// When PartnerConnection is instantiated, a login is implicitly
			// executed and, if successful,
			// a valid session is stored in the ConnectorConfig instance.
			// Use this key to initialize a BulkConnection:
			ConnectorConfig config = new ConnectorConfig();
			config.setSessionId(partnerConfig.getSessionId());
			
			// The endpoint for the Bulk API service is the same as for the normal
			// SOAP uri until the /Soap/ part. From here it's '/async/versionNumber'
			String soapEndpoint = partnerConfig.getServiceEndpoint();
			String apiVersion = "39.0";
			String restEndpoint = soapEndpoint.substring(0, soapEndpoint.indexOf("Soap/")) + "async/" + apiVersion;
			config.setRestEndpoint(restEndpoint);
			
			// This should only be false when doing debugging.
			config.setCompression(true);
			// Set this to true to see HTTP requests and responses on stdout
			config.setTraceMessage(false);
			BulkConnection connection = new BulkConnection(config);
			return connection;
			
		} catch( Exception ex){
			System.err.println( "Error logging into the system - " + ex.getMessage());
			System.err.println( "If you changed password don't forget to change security token as well.");
			System.exit(1);
			return null;
		}
	}

	/**
	 * Create the Connection used to call Describe operations.
	 */
	private PartnerConnection getConnection(String userName, String password, boolean sandbox)
			throws ConnectionException, AsyncApiException {
		try {
			ConnectorConfig partnerConfig = new ConnectorConfig();
	
			partnerConfig.setUsername(userName);
			partnerConfig.setPassword(password);
			if ( sandbox == true)  {
				System.out.println("Connecting to Salesforce Sandbox");
				partnerConfig.setAuthEndpoint("https://test.salesforce.com/services/Soap/u/39.0");
			} else {
				System.out.println("Connecting to Salesforce Production");
				partnerConfig.setAuthEndpoint("https://login.salesforce.com/services/Soap/u/39.0");			
			}
			// Creating the connection automatically handles login and stores
			// the session in partnerConfig
			PartnerConnection connection = new PartnerConnection(partnerConfig);

			GetServerTimestampResult result = connection.getServerTimestamp();
			serverTime = result.getTimestamp();

			return connection;

			// When PartnerConnection is instantiated, a login is implicitly
			// executed and, if successful,
			// a valid session is stored in the ConnectorConfig instance.		
		} catch( Exception ex){
			System.err.println( "Error logging into the system - " + ex.getMessage());
			System.err.println( "If you changed password don't forget to change security token as well.");
			System.exit(1);
			return null;
		}

	}
	private static LastState retrieveStateFile(String dataPath) {
		File stateFile = new File(dataPath + File.separator + "in" + File.separator + "state.json");
        LastState lastState = null;
        if (stateFile.exists()) {
            try {
                lastState = (LastState) JsonConfigParser.parseFile(stateFile, LastState.class);
            } catch (IOException ex) {
                System.err.println( ex + " " + ex.getStackTrace()[0].getLineNumber());
            }
        } else {
            System.out.println("State file does not exist. (first run?)");
        }
        return lastState;
	}
}